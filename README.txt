Selz Module

Features
--------
This module is about a simple way to embed a button or widget from a Selz 
product and display it with your content.
It simply provides a field formatter for link fields. With this formatter, 
you can configure the Selz settings in the manage display tab. 
Then, you can use it entering a Selz link (http://selz.co/xxxxxxx) and 
a title (used in the Call to Action button) for this field type.

You can find the link of a specific item in the embed options inside the
short link tab in the Selz's backend.

Note that this is just a field formatter for Link fields, not a field.

Dependencies
-----------
- link
- jquery_colorpicker
